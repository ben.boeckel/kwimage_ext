"""
Binary extensions for kwimage
"""
__version__ = '0.2.1'

__author__ = 'Kitware Inc., Jon Crall'
__author_email__ = 'kitware@kitware.com, jon.crall@kitware.com'
__url__ = 'https://gitlab.kitware.com/computer-vision/kwimage_ext'

__mkinit__ = """
mkinit -m kwimage_ext
"""
