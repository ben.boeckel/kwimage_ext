:gitlab_url: https://gitlab.kitware.com/computer-vision/kwimage_ext

.. The large version wont work because github strips rst image rescaling. https://i.imgur.com/AcWVroL.png
    # TODO: Add a logo
    .. image:: https://i.imgur.com/PoYIsWE.png
       :height: 100px
       :align: left

Welcome to kwimage_ext's documentation!
=======================================

.. The __init__ files contains the top-level documentation overview
.. automodule:: kwimage_ext.__init__
   :show-inheritance:

.. toctree::
   :maxdepth: 5

   kwimage_ext


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`